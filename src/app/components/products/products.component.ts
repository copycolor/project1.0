import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ProductService } from '../../services/Product.service';

import { Product } from '../../models/Product';

@Component({
  selector: 'app-Products',
  templateUrl: './Products.component.html',
  styleUrls: ['./Products.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {
  Products: Product[];
  editState: boolean = false;
  ProductToEdit: Product;
  
  constructor(public ProductService: ProductService) { }

  ngOnInit() {
    this.ProductService.getProducts().subscribe(Products => {
      //console.log(Products);
      this.Products = Products;
    });
  }

  deleteProduct(event, Product) {
    const response = confirm('are you sure you want to delete?');
    if (response) {
      this.ProductService.deleteProduct(Product);
      location.reload();
    
    }
    return;
  }

  editProduct(event, Product) {
    this.editState = !this.editState;
    this.ProductToEdit = Product;
   
  }

  updateProduct(Product) {
    this.ProductService.updateProduct(Product);
    this.ProductToEdit = null;
    this.editState = false;
  }

  today: number = Date.now();
}
